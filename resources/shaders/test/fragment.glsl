#version 330
out vec4 fragColor;

in vec2 texCoord;
in vec3 normal;

uniform sampler2D diffuse0;

void main() {
    fragColor = texture(diffuse0, texCoord);
}
