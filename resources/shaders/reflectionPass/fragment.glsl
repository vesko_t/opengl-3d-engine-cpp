#version 330 core

out vec4 fragColor;

in vec2 texCoord;

uniform sampler2D diffuse0;

void main()
{
    fragColor = texture(diffuse0, texCoord);
}