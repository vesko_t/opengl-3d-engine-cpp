#version 330
layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gPosition;

in vec3 color;
in vec2 texCoord;
in vec3 normal;
in vec4 crntPos;
in vec4 fragPosLight;
in float visibility;
in vec4 fragPos;


uniform sampler2D diffuse0;
uniform sampler2D specular0;
uniform sampler2D shadowMap;
uniform samplerCube shadowCubeMap;

uniform vec4 lightColor;
uniform vec3 lightPos;
uniform vec3 camPos;
uniform float farPlane;
uniform float camFarPlane;
uniform vec3 skyColor;

vec4 pointLight(){

    vec3 lightVec = lightPos - crntPos.xyz;

    // intensity of light with respect to distance
    float dist = length(lightVec);
    float a = 0.003;
    float b = 0.0002;
    float inten = 1.0f / (a * dist * dist + b * dist + 1.0f);

    // ambient lighting
    float ambient = 0.20f;

    // diffuse lighting
    vec3 norm = normalize(normal);
    vec3 lightDirection = normalize(lightVec);
    float diffuse = max(dot(normal, lightDirection), 0.0f);

    // specular lighting
    float specular = 0.0f;
    if (diffuse != 0.0f)
    {
        float specularLight = 0.50f;
        vec3 viewDirection = normalize(camPos - crntPos.xyz);
        vec3 halfwayVec = normalize(viewDirection + lightDirection);
        float specAmount = pow(max(dot(norm, halfwayVec), 0.0f), 16);
        specular = specAmount * specularLight;
    };

    // Shadow value
    float shadow = 0.0f;
    vec3 fragToLight = crntPos.xyz - lightPos;
    float currentDepth = length(fragToLight);
    float bias = max(0.5f * (1.0f - dot(norm, lightDirection)), 0.0005f);

    // Not really a radius, more like half the width of a square
    int sampleRadius = 3;
    float offset = 0.02f;
    for (int z = -sampleRadius; z <= sampleRadius; z++)
    {
        for (int y = -sampleRadius; y <= sampleRadius; y++)
        {
            for (int x = -sampleRadius; x <= sampleRadius; x++)
            {
                float closestDepth = texture(shadowCubeMap, fragToLight + vec3(x, y, z) * offset).r;
                // Remember that we divided by the farPlane?
                // Also notice how the currentDepth is not in the range [0, 1]
                closestDepth *= farPlane;
                if (currentDepth > closestDepth + bias)
                shadow += 1.0f;
            }
        }
    }
    // Average shadow
    shadow /= pow((sampleRadius * 2 + 1), 3);

    return (texture(diffuse0, texCoord) * (diffuse * (1.0f - shadow) * inten + ambient) + texture(specular0, texCoord).r * specular * (1.0f - shadow) * inten) * lightColor;
}

vec4 directionalLight(){
    // ambient lighting
    float ambient = 0.20f;

    // diffuse lighting
    vec3 norm = normalize(normal);
    vec3 lightDirection = normalize(lightPos);
    float diffuse = max(dot(norm, lightDirection), 0.0f);

    // specular lighting
    float specular = 0.0f;
    if (diffuse != 0.0f)
    {
        float specularLight = 0.50f;
        vec3 viewDirection = normalize(camPos - crntPos.xyz);
        vec3 halfwayVec = normalize(viewDirection + lightDirection);
        float specAmount = pow(max(dot(norm, halfwayVec), 0.0f), 16);
        specular = specAmount * specularLight;
    };


    // Shadow value
    float shadow = 0.0f;
    // Sets lightCoords to cull space
    vec3 lightCoords = fragPosLight.xyz / fragPosLight.w;
    if (lightCoords.z <= 1.0f)
    {
        // Get from [-1, 1] range to [0, 1] range just like the shadow map
        lightCoords = (lightCoords + 1.0f) / 2.0f;
        float currentDepth = lightCoords.z;
        // Prevents shadow acne
        float bias = max(0.025f * (1.0f - dot(norm, lightDirection)), 0.0005f);

        // Smoothens out the shadows
        int sampleRadius = 2;
        vec2 pixelSize = 1.0 / textureSize(shadowMap, 0);
        for (int y = -sampleRadius; y <= sampleRadius; y++)
        {
            for (int x = -sampleRadius; x <= sampleRadius; x++)
            {
                float closestDepth = texture(shadowMap, lightCoords.xy + vec2(x, y) * pixelSize).r;
                if (currentDepth > closestDepth + bias)
                shadow += 1.0f;
            }
        }
        // Get average shadow
        shadow /= pow((sampleRadius * 2 + 1), 2);

    }

    return (texture(diffuse0, texCoord) * (diffuse * (1.0f - shadow) + ambient) + texture(specular0, texCoord).r * specular  * (1.0f - shadow)) * lightColor;
}

vec4 spotLight(){
   float outerCone = 0.90f;
	float innerCone = 0.95f;

	// ambient lighting
	float ambient = 0.20f;

	// diffuse lighting
	vec3 norm = normalize(normal);
	vec3 lightDirection = normalize(lightPos - crntPos.xyz);
	float diffuse = max(dot(norm, lightDirection), 0.0f);

	// specular lighting
	float specular = 0.0f;
	if (diffuse != 0.0f)
	{
		float specularLight = 0.50f;
		vec3 viewDirection = normalize(camPos - crntPos.xyz);
		vec3 halfwayVec = normalize(viewDirection + lightDirection);
		float specAmount = pow(max(dot(norm, halfwayVec), 0.0f), 16);
		specular = specAmount * specularLight;
	};

	// calculates the intensity of the crntPos based on its angle to the center of the light cone
	float angle = dot(vec3(0.0f, -1.0f, 0.0f), -lightDirection);
	float inten = clamp((angle - outerCone) / (innerCone - outerCone), 0.0f, 1.0f);


	// Shadow value
	float shadow = 0.0f;
	// Sets lightCoords to cull space
	vec3 lightCoords = fragPosLight.xyz / fragPosLight.w;
	if(lightCoords.z <= 1.0f)
	{
		// Get from [-1, 1] range to [0, 1] range just like the shadow map
		lightCoords = (lightCoords + 1.0f) / 2.0f;
		float currentDepth = lightCoords.z;
		// Prevents shadow acne
		float bias = max(0.00025f * (1.0f - dot(norm, lightDirection)), 0.000005f);

		// Smoothens out the shadows
		int sampleRadius = 2;
		vec2 pixelSize = 1.0 / textureSize(shadowMap, 0);
		for(int y = -sampleRadius; y <= sampleRadius; y++)
		{
		    for(int x = -sampleRadius; x <= sampleRadius; x++)
		    {
		        float closestDepth = texture(shadowMap, lightCoords.xy + vec2(x, y) * pixelSize).r;
				if (currentDepth > closestDepth + bias)
					shadow += 1.0f;     
		    }    
		}
		// Get average shadow
		shadow /= pow((sampleRadius * 2 + 1), 2);

	}

	return (texture(diffuse0, texCoord) * (diffuse * (1.0f - shadow) * inten + ambient) + texture(specular0, texCoord).r * specular * (1.0f - shadow) * inten) * lightColor;
}

void main() {
    fragColor = pointLight();
    gNormal = normalize(normal);
    gPosition = fragPos;
    //fragColor = mix(vec4(skyColor, 1.0f), fragColor, visibility);
    //fragColor = vec4(gNormal,1);
    float z = gl_FragCoord.z * 2.0 - 1.0;
    float near = 1;
    float far = camFarPlane;
    gl_FragDepth = ((2.0 * near * far) / (far + near - z * (far - near))) / far;
}
