#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTex;

out vec3 color;
out vec2 texCoord;
out vec3 normal;
out vec4 crntPos;
out vec4 fragPosLight;
out float visibility;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 lightProjection;

const float dencity = 0.008;
const float gradient = 1.5;

void main() {
    crntPos = model * vec4(aPos, 1.0);
    vec4 postToCam = view * crntPos;
    gl_Position = projection * postToCam;

    fragPosLight = lightProjection * crntPos;
    texCoord = aTex;
    normal = aNormal;

    float distance = length(postToCam.xyz);
    visibility = exp(-pow(distance * dencity, gradient));
    visibility = clamp(visibility, 0.0, 1.0);
}
