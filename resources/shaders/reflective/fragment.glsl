#version 330 core

layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gPosition;

uniform sampler2D diffuse0;
uniform samplerCube reflectionMap;
uniform float camFarPlane;

in vec2 texCoord;
in vec3 normal;
in vec3 reflectedVector;
in vec3 refractedVector;

void main()
{
    fragColor = texture(diffuse0,texCoord);
    vec4 reflectedColor = texture(reflectionMap,reflectedVector);
    vec4 refractedColor = texture(reflectionMap,refractedVector);

    vec4 enviroColor = mix(reflectedColor,refractedColor, 0.7);

    fragColor = mix(fragColor,enviroColor,0.8);
    gNormal = (normalize(normal)+1.0)/2.0;
    gPosition = (normalize(gl_FragCoord)+1.0)/2.0;
    //fragColor = mix(vec4(skyColor, 1.0f), fragColor, visibility);
    //fragColor = vec4(gNormal,1);
    float z = gl_FragCoord.z * 2.0 - 1.0;
    float near = 1;
    float far = camFarPlane;
    gl_FragDepth = ((2.0 * near * far) / (far + near - z * (far - near))) / far;
}