#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTex;

out vec2 texCoord;
out vec3 normal;
out vec3 reflectedVector;
out vec3 refractedVector;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 camPos;

void main()
{
    vec4 crntPos = model * vec4(aPos, 1.0);
    vec4 postToCam = view * crntPos;
    gl_Position = projection * postToCam;

    texCoord = aTex;
    normal = aNormal;

    vec3 unitNormal = normalize(normal);
    vec3 viewVector = normalize(crntPos.xyz - camPos);
    reflectedVector = reflect(viewVector,unitNormal);
    refractedVector = refract(viewVector,unitNormal,1.0 / 1.52);

}