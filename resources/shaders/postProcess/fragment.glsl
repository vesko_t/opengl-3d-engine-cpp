#version 330
out vec4 fragColor;

in vec2 texCoord;

uniform sampler2D diffuse0;
uniform float contrast;
uniform float brightness;
uniform float saturation;
uniform vec3 lightPosition;
uniform samplerCube shadowCubeMap;
uniform sampler2D depthMap;
uniform sampler2D normalMap;
uniform sampler2D positionMap;
uniform mat4 inverseView;
uniform float zFar;
uniform float zNear;
uniform vec3 cameraPos;
uniform vec2 resolution;

const vec3 lightColor = vec3(1);

const float g = 0;

in vec2 farPlanePos;

vec3 satur(vec3 rgb, float adjustment){
    const vec3 W = vec3(0.2125, 0.7154, 0.0721);
    vec3 intensity = vec3(dot(rgb, W));
    return mix(intensity, rgb, adjustment);
}

vec3 pointLight(){

    vec3 crntPos = texture(positionMap,texCoord).xyz;
    vec3 normal = texture(normalMap,texCoord).xyz;
    vec3 lightVec = lightPosition - crntPos.xyz;

    // intensity of light with respect to distance
    float dist = length(lightVec);
    float a = 0.003;
    float b = 0.0002;
    float inten = 1.0f / (a * dist * dist + b * dist + 1.0f);

    // ambient lighting
    float ambient = 0.20f;

    // diffuse lighting
    vec3 norm = normalize(normal);
    vec3 lightDirection = normalize(lightVec);
    float diffuse = max(dot(normal, lightDirection), 0.0f);

    // specular lighting
    float specular = 0.0f;
    if (diffuse != 0.0f)
    {
        float specularLight = 0.50f;
        vec3 viewDirection = normalize(cameraPos - crntPos.xyz);
        vec3 halfwayVec = normalize(viewDirection + lightDirection);
        float specAmount = pow(max(dot(norm, halfwayVec), 0.0f), 16);
        specular = specAmount * specularLight;
    };

    // Shadow value
    float shadow = 0.0f;
    vec3 fragToLight = crntPos.xyz - lightPosition;
    float currentDepth = length(fragToLight);
    float bias = max(0.5f * (1.0f - dot(norm, lightDirection)), 0.0005f);

    // Not really a radius, more like half the width of a square
    int sampleRadius = 3;
    float offset = 0.02f;
    for (int z = -sampleRadius; z <= sampleRadius; z++)
    {
        for (int y = -sampleRadius; y <= sampleRadius; y++)
        {
            for (int x = -sampleRadius; x <= sampleRadius; x++)
            {
                float closestDepth = texture(shadowCubeMap, fragToLight + vec3(x, y, z) * offset).r;
                // Remember that we divided by the farPlane?
                // Also notice how the currentDepth is not in the range [0, 1]
                closestDepth *= zFar;
                if (currentDepth > closestDepth + bias)
                    shadow += 1.0f;
            }
        }
    }
    // Average shadow
    shadow /= pow((sampleRadius * 2 + 1), 3);

    return ((diffuse * (1.0f - shadow) * inten + ambient) + specular * (1.0f - shadow) * inten) * lightColor;
}

bool isInShadow(vec3 position){
    vec3 fragToLight = position - lightPosition;
    float currentDepth = length(fragToLight);
    float closestDepth = texture(shadowCubeMap, fragToLight).r;
    closestDepth *= zFar;
    return currentDepth > closestDepth;
}

void main() {
    fragColor = vec4(satur(texture(diffuse0,texCoord).rgb, saturation) - contrast * 0.5 + brightness,1);
    //fragColor += vec4(result, 1);
    //fragColor = texture(normalMap, texCoord);
    fragColor *= vec4(pointLight(),1);
}
