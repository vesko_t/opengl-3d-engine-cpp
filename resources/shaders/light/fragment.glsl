#version 330 core

out vec4 FragColor;

uniform vec4 lightColor;
uniform float camFarPlane;

void main()
{
	FragColor = lightColor;
	 float z = gl_FragCoord.z * 2.0 - 1.0;
    float near = 1;
    float far = camFarPlane;
    gl_FragDepth = ((2.0 * near * far) / (far + near - z * (far - near))) / far;
}