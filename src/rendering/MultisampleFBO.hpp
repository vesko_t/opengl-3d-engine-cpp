#ifndef MULTISAMPLE_FBO_H
#define MULTISAMPLE_FBO_H

#include <glad/glad.h>

#include <string>
#include <iostream>

class MultisampleFBO
{
private:
    int width;
    int height;
    unsigned int id;
    unsigned int rbo;
    unsigned int textureId;
    GLuint depthId;
    GLuint normalId;
    GLuint positionId;
    GLuint depthRbo;

public:
    MultisampleFBO();
    MultisampleFBO(int width, int height, int samples);
    ~MultisampleFBO();

    void bind();
    void unbind();
    void deleteObject();
    unsigned int getId();
    unsigned int getTextureId();
    unsigned int getRBO();
    void bindDepthMap(GLuint sampler);
};

#endif // !MULTISAMPLE_FBO_H