#ifndef VBO_H
#define VBO_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <vector>

class Vertex
{
public:
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texUV;

    Vertex();
    Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texUV);
    ~Vertex();
};

class VBO
{
private:
    GLuint id;

public:
    VBO(std::vector<Vertex> &vertices);
    ~VBO();

    void bind();
    void unbind();
    void deleteObject();
    int getId();
};

#endif //