#include "PointLight.hpp"

PointLight::PointLight(Shader &shader, float farPlane, glm::vec3 position) : shader{shader}, farPlane{farPlane}
{
    fbo = DepthFBO(2048, 2048, GL_TEXTURE_CUBE_MAP);
    setPosition(position);
}

PointLight::~PointLight()
{
}

void PointLight::setPosition(glm::vec3 position)
{
    this->position = position;
    shadowTransforms.clear();
    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, farPlane);
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));
    shader.bind();
    for (int i = 0; i < shadowTransforms.size(); i++)
    {
        shader.setUniform("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    }
    shader.setUniform("lightPos", position);
    shader.setUniform("farPlane", farPlane);
}

void PointLight::bind()
{
    shader.bind();
    fbo.bind();
}

void PointLight::unbind()
{
    fbo.unbind();
}

void PointLight::deleteObject()
{
    fbo.deleteObject();
    shader.deleteShader();
}

Shader PointLight::getShader()
{
    return shader;
}

void PointLight::bindDepthMap(int sampler)
{
    glActiveTexture(GL_TEXTURE0 + sampler);
    glBindTexture(fbo.getTextureType(), fbo.getTextureId());
}

float PointLight::getFarPlane()
{
    return farPlane;
}