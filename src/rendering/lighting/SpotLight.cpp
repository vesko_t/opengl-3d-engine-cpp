#include "SpotLight.hpp"

SpotLight::SpotLight(Shader &shader, float farPlane, const glm::vec3 &position, const float &rotation, const glm::vec3 &axis) : shader{shader}, farPlane{farPlane}
{
    fbo = DepthFBO(2048, 2048, GL_TEXTURE_2D);
    setPosition(position, rotation, axis);
}

SpotLight::~SpotLight()
{
}

void SpotLight::setPosition(const glm::vec3 &position, const float &rotation, const glm::vec3 &axis)
{
    this->position = position;

    this->lightProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, farPlane);
    glm::mat4 view = glm::lookAt(position, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    shader.bind();
    shader.setUniform("lightProjection", lightProjection * view);
    shader.setUniform("lightPos", position);
    shader.setUniform("farPlane", farPlane);
}

void SpotLight::bind()
{
    shader.bind();
    fbo.bind();
}

void SpotLight::unbind()
{
    fbo.unbind();
}

void SpotLight::deleteObject()
{
    fbo.deleteObject();
    shader.deleteShader();
}

Shader SpotLight::getShader()
{
    return shader;
}

void SpotLight::bindDepthMap(int sampler)
{
    glActiveTexture(GL_TEXTURE0 + sampler);
    glBindTexture(fbo.getTextureType(), fbo.getTextureId());
}

float SpotLight::getFarPlane()
{
    return farPlane;
}