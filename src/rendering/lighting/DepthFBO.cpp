#include "DepthFBO.hpp"

DepthFBO::DepthFBO() {}

DepthFBO::DepthFBO(int width, int height, GLenum textureType) : width{width}, height{height}, textureType{textureType}
{
    glGenFramebuffers(1, &id);
    glGenTextures(1, &textureId);
    glBindTexture(textureType, textureId);

    if (textureType == GL_TEXTURE_CUBE_MAP)
    {
        glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(textureType, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        for (int i = 0; i < 6; ++i)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        }
    }
    else
    {
        glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, id);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, textureId, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

DepthFBO::~DepthFBO()
{
}

void DepthFBO::bind()
{
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, id);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

void DepthFBO::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthFBO::deleteObject()
{
    glDeleteFramebuffers(1, &id);
}

unsigned int DepthFBO::getId()
{
    return id;
}

unsigned int DepthFBO::getTextureId()
{
    return textureId;
}

GLenum DepthFBO::getTextureType()
{
    return this->textureType;
}