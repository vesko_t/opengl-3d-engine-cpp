#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H
#pragma once

#include "../Shader.hpp"
#include "DepthFBO.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class SpotLight
{
private:
	Shader &shader;
	float farPlane;
	DepthFBO fbo;
	glm::vec3 position;

public:
	glm::mat4 lightProjection;

	SpotLight(Shader &shader, float farPlane, const glm::vec3 &position, const float &rotation, const glm::vec3 &axis);
	~SpotLight();

	void setPosition(const glm::vec3 &position, const float &rotation, const glm::vec3 &axis);

	void bind();

	void unbind();

	void deleteObject();

	Shader getShader();

	void bindDepthMap(int sampler);

	float getFarPlane();
};
#endif