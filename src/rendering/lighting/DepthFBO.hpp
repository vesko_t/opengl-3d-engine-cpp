#ifndef DEPTH_FBO_H
#define DEPTH_FBO_H

#include <glad/glad.h>

class DepthFBO
{
private:
    int width;
    int height;
    unsigned int id;
    unsigned int textureId;
    GLenum textureType;

public:
    DepthFBO();
    DepthFBO(int width, int height, GLenum textureType);
    ~DepthFBO();

    void bind();
    void unbind();
    void deleteObject();
    unsigned int getId();
    unsigned int getTextureId();
    GLenum getTextureType();
};
#endif