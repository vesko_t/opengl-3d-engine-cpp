#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "../Shader.hpp"
#include "DepthFBO.hpp"
#include <vector>

class PointLight
{
private:
    Shader &shader;
    std::vector<glm::mat4> shadowTransforms;
    float farPlane;
    DepthFBO fbo;
    glm::vec3 position;

public:
    PointLight(Shader &shader, float farPlane, glm::vec3 position);
    ~PointLight();

    void setPosition(glm::vec3 position);

    void bind();

    void unbind();

    void deleteObject();

    Shader getShader();

    void bindDepthMap(int sampler);

    float getFarPlane();
};
#endif // !POINT_LIGHT_H
