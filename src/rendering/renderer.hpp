#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include "Camera.hpp"
#include <memory>
#include "MeshRenderer.hpp"
#include "FBO.hpp"

static FBO reflectionFbo;
static std::vector<std::shared_ptr<MeshRenderer>> renderers;
static std::vector<std::shared_ptr<MeshRenderer>> reflectiveRenderers;
static std::shared_ptr<Camera> camera;
static std::vector<glm::mat4> viewTransforms;

void addRenderer(std::shared_ptr<MeshRenderer> meshRenderer);

void addReflectiveRenderer(std::shared_ptr<MeshRenderer> MeshRenderer);

Shader createShader(const std::string &filepath);

void renderScene(Shader &shader, Camera &camera);

void renderReflectionMap(Shader &shader, const glm::vec3 &position, Camera &camera);

void renderReflective(Shader &shader, Camera &camera);

void initRendering();

#endif