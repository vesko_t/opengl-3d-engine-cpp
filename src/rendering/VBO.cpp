
#include "VBO.hpp"

VBO::VBO(std::vector<Vertex> &vertices)
{
    glGenBuffers(1, &id);
    glBindBuffer(GL_ARRAY_BUFFER, id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
}

VBO::~VBO()
{
}

void VBO::bind()
{
    glBindBuffer(GL_ARRAY_BUFFER, id);
}
void VBO::unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void VBO::deleteObject()
{
    glDeleteBuffers(1, &id);
}
int VBO::getId()
{
    return this->id;
}

Vertex::Vertex() : position{glm::vec3()}, normal{glm::vec3()}, texUV{glm::vec2()}
{
}

Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texUV) : position{position}, normal{normal}, texUV{texUV}
{
}

Vertex::~Vertex(){

}