#include "MeshRenderer.hpp"
#include "../engine/EngineObject.hpp"

MeshRenderer::MeshRenderer(const std::shared_ptr<EngineObject> &parrent, const std::shared_ptr<Mesh> &mesh) : EngineComponent(parrent),
                                                                                                              mesh(mesh)
{
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::render(Shader &shader, Camera &camera)
{
    shader.bind();

    shader.setUniform("model", parrent->getTransform()->getMatrix());
    mesh->draw(shader, camera);
}