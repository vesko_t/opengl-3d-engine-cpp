#include "VAO.hpp"

VAO::VAO(/* args */)
{
    glGenVertexArrays(1, &this->id);
}

VAO::~VAO()
{
}

void VAO::linkAttrib(VBO &vbo, GLuint layout, GLuint numComponents, GLenum type, GLsizei stride, void *offset)
{
    vbo.bind();
    glVertexAttribPointer(layout, numComponents, type, GL_FALSE, stride, offset);
    glEnableVertexAttribArray(layout);
    vbo.unbind();
}

void VAO::bind()
{
    glBindVertexArray(id);
}

void VAO::unbind()
{
    glBindVertexArray(0);
}

void VAO::deleteObject ()
{
    glDeleteVertexArrays(1,&id);
}

int VAO::getId()
{
    return id;
}