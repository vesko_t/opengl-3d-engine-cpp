#ifndef SHADER_H
#define SHADER_H

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cerrno>

std::string get_file_contents(const char *filename);

class Shader
{
private:
    int programId;
    int vertexId;
    int fragmentId;
    int geometryId;

public:
    Shader(std::string vertexFile, std::string fragmentFile);
    Shader(std::string vertexFile, std::string geometryFile, std::string fragmentFile);
    ~Shader();

    void vertexSource(std::string source);

    void fragmentSource(std::string source);

    void geometrySource(std::string source);

    void linkShader();

    void bind();

    void unbind();

    void setUniform(std::string uniformName, int value);

    void setUniform(std::string uniformName, float value);

    void setUniform(std::string uniformName, glm::vec4);

    void setUniform(std::string uniformName, glm::vec3);

    void setUniform(std::string uniformName, glm::vec2);

    void setUniform(std::string uniformName, glm::mat4);

    void deleteShader();
};
#endif