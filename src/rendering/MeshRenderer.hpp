#ifndef MESH_RENDERER_H
#define MESH_RENDERER_H

#include <memory>

#include "../rendering/Mesh.hpp"
#include "../engine/EngineComponent.hpp"

class MeshRenderer : public EngineComponent
{
private:
    std::shared_ptr<Mesh> mesh;

public:
    MeshRenderer(const std::shared_ptr<EngineObject> &parrent, const std::shared_ptr<Mesh> &mesh);
    ~MeshRenderer();
    void render(Shader &shader, Camera &camera);
};

#endif