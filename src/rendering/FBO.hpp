#ifndef FBO_H
#define FBO_H

#include <glad/glad.h>

class FBO
{
private:
    int width;
    int height;
    unsigned int id;
    unsigned int textureId;
    GLuint depthId;
    GLenum textureType;
    GLuint normalId;
    GLuint positionId;


public:
    FBO();
    FBO(int width, int height, GLenum textureType);
    ~FBO();

    void bind();
    void unbind();
    void deleteObject();
    unsigned int getId();
    unsigned int getTextureId();
    GLenum getTextureType();
    void bindDepthMap(GLuint sampler);
    void bindNormalMap(GLuint sampler);
    void bindPositionMap(GLuint sampler);
};

#endif