#ifndef MESH_H
#define MESH_H

#include "VAO.hpp"
#include "VBO.hpp"
#include "EBO.hpp"
#include "Texture.hpp"
#include "Camera.hpp"
#include "Shader.hpp"

#include <iostream>
#include <assimp/scene.h>
#include <assimp/cimport.h>
#include <assimp/Importer.hpp>
#include <assimp/material.h>
#include <assimp/postprocess.h>

class Mesh
{
private:
    VAO vao;
    std::vector<Texture> textures;
    std::vector<int> indices;
    std::vector<Vertex> vertices;

public:
    Mesh(std::vector<Vertex>, std::vector<int> indices, std::vector<Texture> textures);
    ~Mesh();
    void draw(Shader &shader, Camera &camera);
    void addTexture(const Texture &texture);
};

Mesh loadMesh(std::string filename);

#endif // !MESH_H
