#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>
#include <string>
#include "../stb/stb_image.h"
#include <iostream>

class Texture
{
private:
    unsigned int textureId;

public:
    std::string type;
    int width;
    int height;
    int numColCh;

    Texture(std::string imageFile, std::string type);
    ~Texture();
    void bindToSampler(int sampler);
    std::string getType();
};

#endif