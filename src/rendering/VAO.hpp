#ifndef VAO_H
#define VAO_H

#include <glad/glad.h>
#include "VBO.hpp"
class VAO
{
private:
    GLuint id;

public:
    VAO();
    ~VAO();

    void linkAttrib(VBO &vbo, GLuint layout, GLuint numComponents, GLenum type, GLsizei stride, void *offset);
    void bind();
    void unbind();
    void deleteObject();
    int getId();
};

#endif // !VAO_H
