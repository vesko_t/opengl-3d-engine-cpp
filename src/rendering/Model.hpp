#ifndef MODEL_H
#define MODEL_H

#include "Mesh.hpp"

class Model
{
private:
    std::vector<Mesh> meshes;
    glm::vec3 position;
    glm::quat rotation;
    glm::vec3 scale;

public:
    Model(std::vector<Mesh> meshes, glm::vec3 position, glm::vec3 scale, glm::quat rotation);
    Model(std::vector<Mesh> meshes, glm::vec3 position, glm::vec3 scale);
    Model(std::vector<Mesh> meshes, glm::vec3 position);
    Model(std::vector<Mesh> meshes);
    ~Model();

    void draw(Shader &shader, Camera &camera);
    glm::vec3 getPosition();
    void setPosition(glm::vec3 position);
    glm::vec3 getScale();
    void setScale(glm::vec3 scale);
    glm::quat getRotation();
    void setRotation(glm::quat rotation);
    void addMesh(Mesh &mesh);
    
};

#endif // !MODEL_H