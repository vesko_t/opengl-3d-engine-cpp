#define STB_IMAGE_IMPLEMENTATION
#include "Texture.hpp"

Texture::Texture(std::string imageFile, std::string type) : textureId{0}, width{0}, height{0}, type{type}
{
    std::cout << imageFile << std::endl;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *texture = stbi_load(imageFile.c_str(), &width, &height, &numColCh, 0);

    if (texture == NULL)
    {
        std::cout << "No tex" << std::endl;
    }

    glGenTextures(1, &textureId);

    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (numColCh == 4)
    {
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            width,
            height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            texture);
    }
    else if (numColCh == 3)
    {
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            width,
            height,
            0,
            GL_RGB,
            GL_UNSIGNED_BYTE,
            texture);
    }
    else if (numColCh == 1)
    {
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            width,
            height,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            texture);
    }
    else
    {
        throw std::invalid_argument("Automatic Texture type recognition failed");
    }
    stbi_image_free(texture);
    glBindTexture(GL_TEXTURE_2D, 0);
}
Texture::~Texture()
{
    //glDeleteTextures(1, &this->textureId);
}
void Texture::bindToSampler(int sampler)
{
    glActiveTexture(GL_TEXTURE0 + sampler);
    glBindTexture(GL_TEXTURE_2D, this->textureId);
}

std::string Texture::getType()
{
    return this->type;
}
