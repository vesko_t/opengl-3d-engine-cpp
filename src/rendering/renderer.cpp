#include "renderer.hpp"

void initRendering()
{
    reflectionFbo = FBO(1024, 1024, GL_TEXTURE_CUBE_MAP);
}

void addRenderer(std::shared_ptr<MeshRenderer> meshRenderer)
{
    renderers.push_back(meshRenderer);
}

void addReflectiveRenderer(std::shared_ptr<MeshRenderer> meshRenderer)
{
    reflectiveRenderers.push_back(meshRenderer);
}

Shader createShader(const std::string &filepath)
{
    return Shader(filepath + "/vertex.glsl", filepath + "/geometry.glsl", filepath + "/fragment.glsl");
}

void renderScene(Shader &shader, Camera &camera)
{
    for (std::shared_ptr<MeshRenderer> renderer : renderers)
    {
        renderer->render(shader, camera);
    }
}

void renderReflectionMap(Shader &shader, const glm::vec3 &position, Camera &camera)
{
    viewTransforms.clear();
    glm::mat4 viewProj = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 1000.0f);
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
    viewTransforms.push_back(viewProj * glm::lookAt(position, position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));
    shader.bind();
    for (int i = 0; i < viewTransforms.size(); i++)
    {
        shader.setUniform("viewMatrices[" + std::to_string(i) + "]", viewTransforms[i]);
    }

    reflectionFbo.bind();
    renderScene(shader, camera);
    reflectionFbo.unbind();
}

void renderReflective(Shader &shader, Camera &camera)
{
    shader.bind();
    shader.setUniform("camPos", camera.position);
    shader.setUniform("reflectionMap", 1);
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(reflectionFbo.getTextureType(), reflectionFbo.getTextureId());

    for (std::shared_ptr<MeshRenderer> renderer : reflectiveRenderers)
    {
        renderer->render(shader, camera);
    }
}