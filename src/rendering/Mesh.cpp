#include "Mesh.hpp"

#include <iostream>

Mesh loadMesh(std::string filename)
{
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(filename, aiProcess_FlipWindingOrder |
                                                           aiProcess_JoinIdenticalVertices |
                                                           aiProcess_OptimizeMeshes |
                                                           aiProcess_Triangulate);
    if (!scene)
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString()
                  << std::endl;
    }
    std::vector<Texture> modelTextures;
    for (unsigned int i = 0; i < scene->mNumMaterials; i++)
    {
        aiMaterial *material = scene->mMaterials[i];

        aiString path;
        if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
        {
            // std::cout << path.C_Str() << std::endl;
            Texture texture(filename.substr(0, filename.find_last_of('/')) + "/" + path.C_Str(), "diffuse");
            modelTextures.push_back(texture);
        }
        if (material->GetTexture(aiTextureType_SPECULAR, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
        {
            Texture texture(filename.substr(0, filename.find_last_of('/')) + "/" + path.C_Str(), "specular");
            modelTextures.push_back(texture);
        }
    }

    std::vector<Vertex> vertices;

    aiMesh *mesh = scene->mMeshes[0];

    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex v;
        const aiVector3D *position = &(mesh->mVertices[i]);
        v.position.x = position->x;
        v.position.y = position->y;
        v.position.z = position->z;

        const aiVector3D *normal = &(mesh->mNormals[i]);
        v.normal.x = normal->x;
        v.normal.y = normal->y;
        v.normal.z = normal->z;

        const aiVector3D *texUV = &(mesh->mTextureCoords[0][i]);
        v.texUV.x = texUV->x;
        v.texUV.y = texUV->y;

        vertices.push_back(v);
    }

    std::vector<int> indices;
    for (unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        const aiFace face = mesh->mFaces[i];
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }

    return Mesh(vertices, indices, modelTextures);
}
//зъб др мартин манчев 088 898 7840
//студ град бл 59 вх г клиника естадент от името на др ина вълова

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<int> indices, std::vector<Texture> textures) : textures{textures}, indices{indices}, vertices{vertices}
{
    vao.bind();

    VBO vbo(vertices);
    EBO ebo(indices);

    vao.linkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void *)0);
    vao.linkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void *)(3 * sizeof(float)));
    vao.linkAttrib(vbo, 2, 2, GL_FLOAT, sizeof(Vertex), (void *)(6 * sizeof(float)));

    vao.unbind();
    vbo.unbind();
    ebo.unbind();
}

Mesh::~Mesh()
{
}

void Mesh::draw(Shader &shader, Camera &camera)
{
    shader.bind();
    vao.bind();

    int numDiffuse = 0;
    int numSpec = 0;

    for (int i = 0; i < textures.size(); i++)
    {
        int num = 0;
        if (textures[i].type == "diffuse")
        {
            num = numDiffuse++;
        }
        else if (textures[i].type == "specular")
        {
            num = numSpec++;
        }
        textures[i].bindToSampler(i);
        shader.setUniform(textures[i].type + std::to_string(num), i);
    }

    camera.matrix(shader);
    shader.setUniform("camPos", camera.position);

    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(indices.size()), GL_UNSIGNED_INT, 0);
    // glDrawElements(GL_TRIANGLES,)
}

void Mesh::addTexture(const Texture &texture)
{
    this->textures.push_back(texture);
}