#ifndef EBO_H
#define EBO_H

#include <glad/glad.h>
#include <vector>
class EBO
{
private:
    GLuint id;

public:
    EBO(std::vector<int> indices);
    ~EBO();

    void bind();
    void unbind();
    void deleteObject();
    int getId();
};

#endif // !EBO_H
