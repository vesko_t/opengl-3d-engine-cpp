#include "Model.hpp"

Model::Model(std::vector<Mesh> meshes, glm::vec3 position, glm::vec3 scale, glm::quat rotation) : meshes{meshes}, position{position}, scale{scale}, rotation{rotation}
{
}
Model::Model(std::vector<Mesh> meshes, glm::vec3 position, glm::vec3 scale) : Model(meshes, position, scale, glm::quat())
{
}
Model::Model(std::vector<Mesh> meshes, glm::vec3 position) : Model(meshes, position, glm::vec3(1), glm::quat())
{
}
Model::Model(std::vector<Mesh> meshes) : Model(meshes, glm::vec3(1), glm::vec3(1), glm::quat())
{
}

Model::~Model()
{
}

void Model::draw(Shader &shader, Camera &camera)
{
    glm::mat4 model = glm::mat4_cast(rotation);
    model = glm::translate(model, position);
    model = glm::scale(model, scale);
    shader.bind();
    shader.setUniform("model", model);

    for (Mesh mesh : meshes)
    {
        mesh.draw(shader, camera);
    }
}

glm::vec3 Model::getPosition()
{
    return position;
}

void Model::setPosition(glm::vec3 position)
{
    this->position = position;
}

glm::vec3 Model::getScale()
{
    return scale;
}

void Model::setScale(glm::vec3 scale)
{
    this->scale = scale;
}

glm::quat Model::getRotation()
{
    return rotation;
}

void Model::setRotation(glm::quat rotation)
{
    this->rotation = rotation;
}

void Model::addMesh(Mesh &mesh)
{
    meshes.push_back(mesh);
}