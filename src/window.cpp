#include "window.hpp"

GLFWwindow *windowRef;
int g_width;
int g_height;

int createWindow(std::string title, int width, int height, bool vsync, bool fullscreen)
{
    g_width = width;
    g_height = height;
    if (!glfwInit())
    {
        glfwTerminate();
        return -1;
    }

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();

    const GLFWvidmode *vidMode = glfwGetVideoMode(monitor);

    windowRef = glfwCreateWindow(fullscreen ? vidMode->width : width,
                                 fullscreen ? vidMode->height : height,
                                 title.c_str(), fullscreen ? monitor : 0, 0);

    if (fullscreen)
    {
        g_width = vidMode->width;
        g_height = vidMode->height;
    }

    if (!windowRef)
    {
        glfwTerminate();
        return -1;
    }

    if (!fullscreen)
    {
        glfwSetWindowPos(windowRef, (vidMode->width - width) / 2, (vidMode->height - height) / 2);
    }

    glfwMakeContextCurrent(windowRef);
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return -1;
    }
    if (vsync)
    {
        glfwSwapInterval(1);
    }
    else
    {
        glfwSwapInterval(0);
    }
}
void windowAfterRender()
{
    glfwSwapBuffers(windowRef);
    glfwPollEvents();
}
bool windowShouldClose()
{
    return glfwWindowShouldClose(windowRef) == 1;
}
void destroyWindow()
{
    glfwDestroyWindow(windowRef);
    glfwTerminate();
}

void setVsync(bool vsync)
{
    if (vsync)
        glfwSwapInterval(1);
}
