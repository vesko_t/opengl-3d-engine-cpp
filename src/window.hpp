#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>


extern GLFWwindow *windowRef;
extern int g_width;
extern int g_height;

int createWindow(std::string title, int width, int height, bool vsync, bool fullscreen);
void windowAfterRender(void);
void destroyWindow(void);
bool windowShouldClose(void);
void setVsync(bool vsync);

#endif