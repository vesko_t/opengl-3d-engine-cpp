#include "terrain.hpp"

Mesh generateTerrain()
{
    std::srand(std::time(nullptr));
    seed = getRandomNumber(0, 1000000000);

    int count = MAX_VERTICES * MAX_VERTICES;

    std::vector<Vertex> vertices;
    vertices.reserve(count);

    std::vector<int> indices;
    indices.reserve(6 * (MAX_VERTICES - 1) * (MAX_VERTICES - 1));

    for (int i = 0; i <= MAX_VERTICES; i++)
    {
        for (int j = 0; j < MAX_VERTICES; j++)
        {
            vertices.emplace_back(glm::vec3((float)j / ((float)MAX_VERTICES - 1) * SIZE,
                                            getHeight(i, j),
                                            (float)i / ((float)MAX_VERTICES - 1) * SIZE),
                                  calculateNormal(i, j), glm::vec2((float)j / ((float)MAX_VERTICES - 1), (float)i / ((float)MAX_VERTICES - 1)));
        }
    }

    for (int gz = 0; gz < MAX_VERTICES - 1; gz++)
    {
        for (int gx = 0; gx < MAX_VERTICES - 1; gx++)
        {
            int topLeft = (gz * MAX_VERTICES) + gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz + 1) * MAX_VERTICES) + gx;
            int bottomRight = bottomLeft + 1;
            indices.push_back(topLeft);
            indices.push_back(bottomLeft);
            indices.push_back(topRight);
            indices.push_back(topRight);
            indices.push_back(bottomLeft);
            indices.push_back(bottomRight);
        }
    }

    return Mesh(vertices, indices, std::vector<Texture>());
}

static glm::vec3 calculateNormal(int x, int z)
{
    float heightL = getHeight(x - 1, z);
    float heightR = getHeight(x + 1, z);
    float heightD = getHeight(x, z - 1);
    float heightU = getHeight(x, z + 1);
    glm::vec3 normal = glm::vec3(heightL - heightR, 2.0f, heightD - heightU);
    normal = glm::normalize(normal);
    return normal;
}

static float getHeight(int x, int z)
{
    float total = 0;
    float d = (float)std::pow(2, OCTAVES - 1);
    for (int i = 0; i < OCTAVES; i++)
    {
        float freq = (float)(std::pow(2, i) / d);
        float amp = (float)std::pow(ROUGHNESS, i) * AMPLITUDE;
        total += getInterpolatedNoise(x * freq, z * freq) * amp;
    }
    return total;
}

static float getInterpolatedNoise(float x, float z)
{
    int intX = (int)x;
    int intZ = (int)z;
    float fracX = x - intX;
    float fracZ = z - intZ;

    float v1 = getSmoothNoise(intX, intZ);
    float v2 = getSmoothNoise(intX + 1, intZ);
    float v3 = getSmoothNoise(intX, intZ + 1);
    float v4 = getSmoothNoise(intX + 1, intZ + 1);
    float i1 = interpolate(v1, v2, fracX);
    float i2 = interpolate(v3, v4, fracX);
    return interpolate(i1, i2, fracZ);
}

static float interpolate(float a, float b, float blend)
{
    double theta = blend * M_PI;
    float f = (float)(1.0f - std::cos(theta)) * 0.5f;
    return a * (1.0f - f) + b * f;
}

static float getSmoothNoise(int x, int z)
{
    float corners = (getNoise(x - 1, z - 1) + getNoise(x + 1, z - 1) + getNoise(x - 1, z + 1) + getNoise(x + 1, z + 1)) / 16.0f;
    float sides = (getNoise(x - 1, z) + getNoise(x + 1, z) + getNoise(x, z - 1) + getNoise(x, z + 1)) / 8.0f;
    float center = getNoise(x, z) / 4.0f;
    return corners + sides + center;
}

static float getNoise(int x, int z)
{
    std::srand(x * 49632 + z * 325176 + seed);
    return ((float)std::rand() / RAND_MAX) * 2.0f - 1.0f;
}

static int getRandomNumber(int min, int max)
{
    return min + rand() % ((max + 1) - min);
}