#ifndef TERRAIN_H
#define TERRAIN_H
#define _USE_MATH_DEFINES
#include "../rendering/Mesh.hpp"
#include <cstdlib>
#include <ctime>
#include <cmath>

static const int MAX_VERTICES = 128;
static const float AMPLITUDE = 40.0f;
static const int OCTAVES = 3;
static const float ROUGHNESS = 0.2f;
static const float SIZE = 800;

static int seed;

Mesh generateTerrain();

static glm::vec3 calculateNormal(int x, int z);

static float getHeight(int x, int z);

static float getInterpolatedNoise(float x, float z);

static float interpolate(float a, float b, float blend);

static float getSmoothNoise(int x, int z);

static float getNoise(int x, int z);

static int getRandomNumber(int min, int max);

#endif