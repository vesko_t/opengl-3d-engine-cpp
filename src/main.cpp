#include "window.hpp"
#include "input.hpp"
#include "rendering/Model.hpp"
#include "rendering/lighting/PointLight.hpp"
#include "rendering/FBO.hpp"
#include "rendering/MultisampleFBO.hpp"
#include "rendering/renderer.hpp"
#include "engine/EngineObject.hpp"
#include "engine/EngineComponent.hpp"
//#include "physics/physics.hpp"
#include "terrain/terrain.hpp"
#include "rendering/lighting/SpotLight.hpp"

#include <glm/gtc/matrix_transform.hpp>

#define WINDOWS
#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

static const int width = 1280;
static const int height = 720;

void GLAPIENTRY
MessageCallback(GLenum source,
				GLenum type,
				GLuint id,
				GLenum severity,
				GLsizei length,
				const GLchar *message,
				const void *userParam)
{
	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
			(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
			type, severity, message);
}

std::string GetCurrentWorkingDir(void)
{
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	std::string current_working_dir(buff);
	return current_working_dir;
}

int main(void)
{
	createWindow("Opengl-3D", width, height, true, false);

	glEnable(GL_DEBUG_OUTPUT);
	//glDebugMessageCallback(MessageCallback, 0);

	std::string curentDir = GetCurrentWorkingDir();

	std::cout << curentDir << std::endl;

	glEnable(GL_DEPTH_TEST);
	// Enables Cull Facing
	/* glEnable(GL_CULL_FACE);
	// Keeps front faces
	glCullFace(GL_FRONT);
	// Uses counter clock-wise standard
	glFrontFace(GL_CCW); */
	glEnable(GL_MULTISAMPLE);

	std::cout
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	//initPhysics();
	//setGravity(glm::vec3(0, -9.81, 0));
	initRendering();

	Shader defaultShader = createShader(curentDir + "/resources/shaders/default");
	Shader lightShader = createShader(curentDir + "/resources/shaders/light");
	Shader shadowCubeMapShader = createShader(curentDir + "/resources/shaders/shadowCubeMap");
	Shader ppShader = createShader(curentDir + "/resources/shaders/postProcess");
	Shader reflectionMapShader = createShader(curentDir + "/resources/shaders/reflectionPass");
	Shader reflectiveShader = createShader(curentDir + "/resources/shaders/reflective");
	Shader shadowMapShader = createShader(curentDir + +"/resources/shaders/shadowMap");

	std::vector<Texture> floorTextures;
	floorTextures.push_back(Texture(curentDir + "/resources/images/planks.png", "diffuse"));
	floorTextures.push_back(Texture(curentDir + "/resources/images/planksSpec.png", "specular"));

	std::vector<Vertex> floorVertices;
	floorVertices.push_back(Vertex{glm::vec3(-1.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f, 0.0f)});
	floorVertices.push_back(Vertex{glm::vec3(-1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f, 1.0f)});
	floorVertices.push_back(Vertex{glm::vec3(1.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(1.0f, 1.0f)});
	floorVertices.push_back(Vertex{glm::vec3(1.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(1.0f, 0.0f)});

	std::vector<int> floorIndices;
	floorIndices.push_back(0);
	floorIndices.push_back(1);
	floorIndices.push_back(2);
	floorIndices.push_back(0);
	floorIndices.push_back(2);
	floorIndices.push_back(3);

	std::vector<Vertex> lightVertices = {
		Vertex{glm::vec3(-0.1f, -0.1f, 0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(-0.1f, -0.1f, -0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(0.1f, -0.1f, -0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(0.1f, -0.1f, 0.1f), glm::vec3(0), glm::vec2(0)},

		Vertex{glm::vec3(-0.1f, 0.1f, 0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(-0.1f, 0.1f, -0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(0.1f, 0.1f, -0.1f), glm::vec3(0), glm::vec2(0)},
		Vertex{glm::vec3(0.1f, 0.1f, 0.1f), glm::vec3(0), glm::vec2(0)}};

	std::vector<int> lightIndices = {
		2, 1, 0,
		3, 2, 0,
		0, 4, 7,
		0, 7, 3,
		3, 7, 6,
		3, 6, 2,
		2, 6, 5,
		2, 5, 1,
		1, 5, 4,
		1, 4, 0,
		4, 5, 6,
		4, 6, 7};

	std::vector<Vertex> ppVert = {
		Vertex{glm::vec3(-1, 1, 0), glm::vec3(0), glm::vec2(0, 1)},
		Vertex{glm::vec3(1, 1, 0), glm::vec3(0), glm::vec2(1, 1)},
		Vertex{glm::vec3(1, -1, 0), glm::vec3(0), glm::vec2(1, 0)},
		Vertex{glm::vec3(-1, -1, 0), glm::vec3(0), glm::vec2(0, 0)},
	};

	std::vector<int> ppIndices = {
		0, 1, 2,
		2, 3, 0};

	Mesh floorMesh(floorVertices, floorIndices, floorTextures);
	std::shared_ptr<EngineObject> floor = std::make_shared<EngineObject>(glm::vec3(0, -10, 0));
	floor->getTransform()->setScale(glm::vec3(40));
	std::shared_ptr<MeshRenderer> floorMeshComponent = std::make_shared<MeshRenderer>(floor, std::make_shared<Mesh>(floorMesh));
	//std::shared_ptr<btCollisionShape> floorShape = std::make_shared<btStaticPlaneShape>(btVector3(btScalar(0), btScalar(1), btScalar(0)), btScalar(0));
	//std::shared_ptr<RigidBody> floorBody = std::make_shared<RigidBody>(floor, 0, floorShape);
	// addBody(floorBody);
	addRenderer(floorMeshComponent);
	floor->addComponent(floorMeshComponent);
	//floor->addComponent(floorBody);

	Mesh swordMesh = loadMesh(curentDir + "/resources/models/sword/scene.gltf");

	EngineObject sword = EngineObject(glm::vec3(0, 2, 0));
	sword.getTransform()->setScale(glm::vec3(0.1f));
	std::shared_ptr<MeshRenderer> swordMeshComponent = std::make_shared<MeshRenderer>(std::make_shared<EngineObject>(sword), std::make_shared<Mesh>(swordMesh));
	sword.addComponent(swordMeshComponent);
	addRenderer(swordMeshComponent);

	/* Model sword(std::vector<Mesh>(), glm::vec3(0.0f, 2.0f, 0.0f), glm::vec3(0.1f));
	sword.addMesh(swordMesh);
 */
	Mesh playerMesh = loadMesh(curentDir + "/resources/models/cube/cube.gltf");
	playerMesh.addTexture(Texture(curentDir + "/resources/images/tile2.png", "diffuse"));
	std::shared_ptr<EngineObject> player = std::make_shared<EngineObject>(glm::vec3(0, 10, 0));
	std::shared_ptr<MeshRenderer> playerMeshComponent = std::make_shared<MeshRenderer>(player, std::make_shared<Mesh>(playerMesh));
	//std::shared_ptr<btCollisionShape> playerShape = std::make_shared<btBoxShape>(btVector3(btScalar(1), btScalar(1), btScalar(1)));
	//std::shared_ptr<RigidBody> playerBody = std::make_shared<RigidBody>(player, 1, playerShape);
	// addBody(playerBody);
	addRenderer(playerMeshComponent);
	player->addComponent(playerMeshComponent);
	// player->addComponent(playerBody);

	Mesh teapotMesh = loadMesh(curentDir + "/resources/models/sphere.obj");
	teapotMesh.addTexture(Texture(curentDir + "/resources/images/meta.png", "diffuse"));
	std::shared_ptr<EngineObject> teapot = std::make_shared<EngineObject>(glm::vec3(5, 5, 0));
	teapot->getTransform()->setScale(glm::vec3(1));
	std::shared_ptr<MeshRenderer> teapotMeshComp = std::make_shared<MeshRenderer>(teapot, std::make_shared<Mesh>(teapotMesh));
	addReflectiveRenderer(teapotMeshComp);
	teapot->addComponent(teapotMeshComp);

	Mesh lightMesh(lightVertices, lightIndices, std::vector<Texture>());
	Model light(std::vector<Mesh>(), glm::vec3(15, 15, 5));
	light.addMesh(lightMesh);
	glm::vec4 lightColor = glm::vec4(1);

	/* Mesh terrainMesh = generateTerrain();
	terrainMesh.addTexture(Texture(curentDir + "/resources/images/tile2.png", "diffuse"));
	glm::mat4 terrainMatrix = glm::mat4();
	terrainMatrix = glm::rotate(terrainMatrix, 180.0f, glm::vec3(1, 0, 0)); */

	Mesh ppMesh(ppVert, ppIndices, std::vector<Texture>());

	MultisampleFBO multisampleFbo(g_width, g_height, 16);

	FBO ppFBO(g_width, g_height, GL_TEXTURE_2D);

	lightShader.bind();
	lightShader.setUniform("lightColor", lightColor);

	defaultShader.bind();
	defaultShader.setUniform("lightPos", light.getPosition());
	defaultShader.setUniform("lightColor", lightColor);

	Camera camera(g_width, g_height, glm::vec3(0.0f, 7.0f, 31.0f));

	PointLight pointLight(shadowCubeMapShader, 100.0f, light.getPosition());

	// SpotLight spotLight(shadowMapShader, 100.0f, light.getPosition(), -90.0f, glm::vec3(0.0f, 0.0f, 1.0f));

	double prevFrame = 0.0;
	double crntFrame = 0.0;
	double timeDiff;
	int counter = 0;

	float contrast = 1.57f;
	float brightness = 0.75f;
	float saturation = 1.65f;

	bool renderReflections = true;
	double reflectionRenderTime = 0.0f;

	while (!windowShouldClose())
	{
		crntFrame = glfwGetTime();
		timeDiff = crntFrame - prevFrame;
		counter++;

		if (timeDiff >= 1.0 / 30.0)
		{
			std::string FPS = std::to_string((1.0 / timeDiff) * counter);
			std::string ms = std::to_string((timeDiff / counter) * 1000);
			std::string newTitle = "OpenGL3D - " + FPS + "FPS / " + ms + "ms";
			glfwSetWindowTitle(windowRef, newTitle.c_str());

			// Resets times and counter
			prevFrame = crntFrame;
			counter = 0;

			camera.inputs(windowRef);
			//updatePhysics(timeDiff);
			sword.update(timeDiff);
			floor->update(timeDiff);
			player->update(timeDiff);
			teapot->update(timeDiff);
		}

		camera.updateMatrix(45, 1.0f, 1000.0f);

		if (renderReflections)
		{
			glViewport(0, 0, g_width, g_height);
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);

			renderReflectionMap(reflectionMapShader, glm::vec3(-5.0f, 5.0f, 0.0f), camera);
			renderReflections = false;
			reflectionRenderTime = 0.0;
		}
		else
		{
			reflectionRenderTime += timeDiff;
			renderReflections = reflectionRenderTime >= 1.0;
		}
		glEnable(GL_DEPTH_TEST);
		glViewport(0, 0, g_width, g_height);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		pointLight.bind();
		// spotLight.bind();
		// sword.draw(shadowCubeMapShader, camera);
		renderScene(shadowCubeMapShader, camera);
		renderReflective(shadowCubeMapShader, camera);/* 
		shadowCubeMapShader.setUniform("model", terrainMatrix);
		terrainMesh.draw(shadowCubeMapShader, camera); */
		pointLight.unbind();
		// spotLight.unbind();

		ppFBO.bind();

		defaultShader.bind();
		defaultShader.setUniform("farPlane", pointLight.getFarPlane());
		defaultShader.setUniform("camFarPlane", 1000.0f);
		defaultShader.setUniform("skyColor", glm::vec3(0.07f, 0.13f, 0.17f));
		// defaultShader.setUniform("lightProjection", spotLight.lightProjection);
		pointLight.bindDepthMap(2);
		defaultShader.setUniform("shadowCubeMap", 2);

		reflectiveShader.bind();
		reflectiveShader.setUniform("camFarPlane", 1000.0f);

		renderScene(defaultShader, camera);
		renderReflective(reflectiveShader, camera);/* 
		defaultShader.setUniform("model", terrainMatrix);
		terrainMesh.draw(defaultShader, camera); */

		/* lightShader.bind();
		lightShader.setUniform("camFarPlane", 100.0f);
		light.draw(lightShader, camera); */

		/* glBindFramebuffer(GL_READ_FRAMEBUFFER, multisampleFbo.getId());
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ppFBO.getId());

		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

		glReadBuffer(GL_COLOR_ATTACHMENT1);
		glDrawBuffer(GL_COLOR_ATTACHMENT1);
		glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

		glReadBuffer(GL_COLOR_ATTACHMENT2);
		glDrawBuffer(GL_COLOR_ATTACHMENT2);
		glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
 */
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glDisable(GL_DEPTH_TEST);

		ppShader.bind();
		ppShader.setUniform("contrast", contrast);
		ppShader.setUniform("brightness", brightness);
		ppShader.setUniform("saturation", saturation);
		ppShader.setUniform("lightPosition", light.getPosition());
		ppShader.setUniform("diffuse0", 0);
		ppShader.setUniform("shadowCubeMap", 1);
		ppShader.setUniform("depthMap", 2);
		ppShader.setUniform("normalMap", 3);
		ppShader.setUniform("positionMap", 4);
		ppShader.setUniform("zFar", 100.0f);
		ppShader.setUniform("zNear", 1.0f);
		ppShader.setUniform("aspect", 16.0f / 9.0f);
		ppShader.setUniform("fov", 45.0f);
		ppShader.setUniform("cameraPos", camera.position);
		ppShader.setUniform("resolution", glm::vec2(g_width, g_height));
		ppShader.setUniform("inverseView", glm::inverse(camera.view));
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ppFBO.getTextureId());
		pointLight.bindDepthMap(1);
		ppFBO.bindDepthMap(2);
		ppFBO.bindNormalMap(3);
		ppFBO.bindPositionMap(4);
		ppMesh.draw(ppShader, camera);

		if (glfwGetKey(windowRef, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(windowRef, true);
		}
		inputUpdate();
		windowAfterRender();
	}

	destroyWindow();

	return 0;
}
