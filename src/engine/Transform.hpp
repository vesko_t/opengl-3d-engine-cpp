#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

class Transform
{
private:
    glm::mat4 matrix;
    glm::vec3 position;
    glm::vec3 scale;
    glm::quat rotation;

public:
    Transform(glm::vec3 position);
    Transform(glm::vec3 position, glm::vec3 scale);
    Transform(glm::vec3 position, glm::vec3 scale, glm::quat rotation);
    ~Transform();

    void updateMatrix();
    void mulMatrix(const Transform &transform);
    glm::mat4 &getMatrix();
    glm::vec3 &getPosition();
    glm::vec3 &getScale();
    glm::quat &getRotation();
    void setPosition(glm::vec3 position);
    void setScale(glm::vec3 scale);
    void setRotation(glm::quat rotation);
};

#endif