#include "EngineObject.hpp"
#include "EngineComponent.hpp"

EngineObject::EngineObject(glm::vec3 position) : id{},
                                                 name{},
                                                 parrent{},
                                                 children{},
                                                 components{},
                                                 transform{std::make_shared<Transform>(position)}
{
}

EngineObject::~EngineObject() {}

void EngineObject::update(float delta)
{
    transform->updateMatrix();
    for (std::shared_ptr<EngineObject> engineObject : children)
    {
        engineObject->update(delta);
        engineObject->getTransform()->mulMatrix(*transform);
    }
}

std::vector<std::shared_ptr<EngineObject>> EngineObject::getChildren()
{
    return this->children;
}

std::vector<std::shared_ptr<EngineComponent>> EngineObject::getComponents()
{
    return this->components;
}

std::shared_ptr<EngineObject> EngineObject::getParrent()
{
    return this->parrent;
}

std::string EngineObject::getId()
{
    return this->id;
}

std::string EngineObject::getName()
{
    return this->name;
}

std::shared_ptr<Transform> EngineObject::getTransform()
{
    return this->transform;
}

void EngineObject::addComponent(std::shared_ptr<EngineComponent> engineComponent)
{
    this->components.push_back(engineComponent);
}
