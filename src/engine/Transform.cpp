#include "Transform.hpp"

Transform::Transform(glm::vec3 position) : Transform(position, glm::vec3(1))
{
}

Transform::Transform(glm::vec3 position, glm::vec3 scale) : Transform(position, scale, glm::quat())
{
}

Transform::Transform(glm::vec3 position, glm::vec3 scale, glm::quat rotation) : position{position}, scale{scale}, rotation{rotation}
{
}

Transform::~Transform()
{
}

void Transform::updateMatrix()
{
    matrix = glm::toMat4(rotation);
    matrix = glm::translate(matrix, position);
    matrix = glm::scale(matrix, scale);
}

void Transform::mulMatrix(const Transform &transform)
{
    this->matrix *= transform.matrix;
}

glm::mat4 &Transform::getMatrix()
{
    return matrix;
}

glm::vec3 &Transform::getPosition()
{
    return this->position;
}

glm::vec3 &Transform::getScale()
{
    return this->scale;
}

glm::quat &Transform::getRotation()
{
    return this->rotation;
}

void Transform::setPosition(glm::vec3 position)
{
    this->position = position;
}

void Transform::setScale(glm::vec3 scale)
{
    this->scale = scale;
}

void Transform::setRotation(glm::quat rotation)
{
    this->rotation = rotation;
}