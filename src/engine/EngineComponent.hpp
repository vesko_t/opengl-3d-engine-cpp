#ifndef ENGINE_COMPONENT_H
#define ENGINE_COMPONENT_H

#include <memory>
#include <string>

class EngineObject;

class EngineComponent
{

public:
    EngineComponent(const std::shared_ptr<EngineObject> &parrent);
    ~EngineComponent();

    void setParrent(const std::shared_ptr<EngineObject> &parrent);
    virtual void update(float delta);

protected:
    std::shared_ptr<EngineObject> parrent;
    std::string id;
    std::string name;
};

#endif