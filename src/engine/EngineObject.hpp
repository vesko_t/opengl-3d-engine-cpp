#ifndef ENGINE_OBJECT_H
#define ENGINE_OBJECT_H

#include <vector>
#include <memory>
#include "Transform.hpp"
#include <string>

class EngineComponent;

class EngineObject : public std::enable_shared_from_this<EngineObject>
{

public:
    EngineObject(glm::vec3 position);
    ~EngineObject();

    void update(float delta);
    std::vector<std::shared_ptr<EngineObject>> getChildren();
    std::vector<std::shared_ptr<EngineComponent>> getComponents();
    std::shared_ptr<EngineObject> getParrent();
    std::string getId();
    std::string getName();
    std::shared_ptr<Transform> getTransform();
    void addComponent(std::shared_ptr<EngineComponent> engineComponent);

protected:
    std::string id;
    std::string name;
    std::shared_ptr<EngineObject> parrent;
    std::vector<std::shared_ptr<EngineObject>> children;
    std::vector<std::shared_ptr<EngineComponent>> components;
    std::shared_ptr<Transform> transform;
};

#endif