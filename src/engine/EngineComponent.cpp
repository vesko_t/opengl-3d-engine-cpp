#include "EngineComponent.hpp"

EngineComponent::EngineComponent(const std::shared_ptr<EngineObject> &parrent) : id(),
                                                                                 name(),
                                                                                 parrent(parrent)
{
}

EngineComponent::~EngineComponent() {}

void EngineComponent::update(float delta)
{
}

void EngineComponent::setParrent(const std::shared_ptr<EngineObject> &parrent)
{
    this->parrent = parrent;
}
