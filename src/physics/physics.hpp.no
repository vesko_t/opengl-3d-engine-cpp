#ifndef PHYSICS_H
#define PHYSICS_H

#include "RigidBody.hpp"
#include <vector>

static btDefaultCollisionConfiguration *collisionConfiguration = nullptr;
static btCollisionDispatcher *collisionDispatcher = nullptr;
static btBroadphaseInterface *overlappingPairCache = nullptr;
static btSequentialImpulseConstraintSolver *solver = nullptr;
static btDiscreteDynamicsWorld *world = nullptr;
static std::vector<std::shared_ptr<RigidBody>> physicsBodies;

void initPhysics();

void destroyPhyscs();

void addBody(const std::shared_ptr<RigidBody> &rigidBody);

void setGravity(const glm::vec3 &gravity);

void updatePhysics(float delta);

#endif